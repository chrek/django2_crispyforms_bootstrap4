from django.urls import path

from records.views import UserCreateView

urlpatterns = [
    path('login', UserCreateView.as_view())
]

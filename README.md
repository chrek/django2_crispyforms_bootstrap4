# django2_crispyforms_bootstrap4
This Django project has one application that is using the django-crispy-forms
Django application with Bootstrap 4 to build, customize and reuse Django built-in form.

----

The main features that have currently been implemented are:

* Render Django's basic form using Bootstrap 4 CSS classes
  
Start the Django dev server with `py manage.py runserver` 

## Access the website

Access the website from the browser using `http://127.0.0.1:8000/records/login`

## Reference

1.	[techiediaries](https://www.techiediaries.com/django-form-bootstrap/)

